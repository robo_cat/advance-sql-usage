-- Provided Table queries to start
-- Create Tables
CREATE TABLE warehouses (
    warehouse_id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    location VARCHAR(100) NOT NULL
);

CREATE TABLE carriers (
    carrier_id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    contact_person VARCHAR(50),
    contact_number VARCHAR(15)
);

CREATE TABLE shipments (
    shipment_id SERIAL PRIMARY KEY,
    tracking_number VARCHAR(20) UNIQUE NOT NULL,
    weight DECIMAL(10, 2) NOT NULL,
    status VARCHAR(20) DEFAULT 'Pending',
    warehouse_id INT REFERENCES warehouses(warehouse_id),
    carrier_id INT REFERENCES carriers(carrier_id)
);

-- Seed Data
INSERT INTO warehouses (name, location) VALUES
    ('Warehouse A', 'Location A'),
    ('Warehouse B', 'Location B');

INSERT INTO carriers (name, contact_person, contact_number) VALUES
    ('Carrier X', 'John Doe', '123-456-7890'),
    ('Carrier Y', 'Jane Smith', '987-654-3210');

INSERT INTO shipments (tracking_number, weight, warehouse_id, carrier_id) VALUES
    ('ABC123', 150.5, 1, 1),
    ('XYZ789', 200.0, 2, 2);
	
	
-- Tasks to support ongoing operations

-- 1. Views
/* warehouse_shipments view displays the tracking number, weight, and status of shipments along with 
the warehouse name for each shipment. INNER JOIN is used for to get the shipments 
where warehouses are included in the result set. */

CREATE VIEW warehouse_shipments AS
SELECT shipments.tracking_number, shipments.weight, shipments.status, warehouses.name AS warehouses_name
FROM shipments
INNER JOIN warehouses ON warehouses.warehouse_id = shipments.warehouse_id;

SELECT * FROM warehouse_shipments;

/* carrier_shipments view displays the tracking number, weight, and status of shipments along with 
the carrier name for each shipment. INNER JOIN is used for to get the shipments 
where carriers are included in the result set. */

CREATE VIEW carrier_shipments AS
SELECT shipments.tracking_number, shipments.weight, shipments.status, carriers.name AS carriers_name
FROM shipments
INNER JOIN carriers ON carriers.carrier_id = shipments.carrier_id;

SELECT * FROM carrier_shipments;

-- 2. CTE

/* pending_shipments CTE displays tracking number, weight, and warehouse location 
for all shipments with the status 'Pending'. INNER JOIN is used for to get the shipments 
where warehouses are included in the result set. */

WITH pending_shipments AS (
    SELECT shipments.tracking_number, shipments.weight, warehouses.location AS warehouses_location
	FROM shipments
	INNER JOIN warehouses ON warehouses.warehouse_id = shipments.warehouse_id
	WHERE shipments.status = 'Pending'
)

SELECT * FROM pending_shipments;

/* pending_shipments CTE displays tracking number, weight, and carrier name 
for all shipments with a weight greater than 200. INNER JOIN is used for to get the shipments 
where carriers are included in the result set. */

WITH heavy_shipments AS (
    SELECT shipments.tracking_number, shipments.weight, carriers.name AS carriers_name
	FROM shipments
	INNER JOIN carriers ON carriers.carrier_id = shipments.carrier_id
	WHERE shipments.weight > 200
)

SELECT * FROM heavy_shipments;

-- Let's add one entry that has more then 200 as weight
INSERT INTO shipments (tracking_number, weight, warehouse_id, carrier_id) VALUES
    ('LMN789', 250.0, 2, 2);
	
-- After adding new entry run the previous CTE and you are going to have the result showing correctly.

-- 3. Transaction

/* Below transaction updates the status of the shipment with tracking number 'ABC123' 
to 'Shipped' and increments the weight by 10 units. COMMIT works like END the transaction */

SELECT * FROM shipments;

BEGIN;

UPDATE shipments
SET status = 'Shipped', weight = weight + 10
WHERE tracking_number = 'ABC123';

COMMIT;

SELECT * FROM shipments;

/* Below transaction inserts a new shipment with 
tracking number 'LMN456', weight 180.75, into Warehouse B using Carrier Y.
COMMIT works like END the transaction */

BEGIN;

INSERT INTO shipments (tracking_number, weight, warehouse_id, carrier_id) VALUES
    ('LMN456', 180.75, 2, 2);

COMMIT;

SELECT * FROM shipments;

-- 4. Indexes

EXPLAIN ANALYSE SELECT tracking_number, weight, status
FROM shipments
WHERE tracking_number = 'LMN456';

/* To improve the search performance we need to create a index and here we do not need any sorting just matching the query
so hash indexes seems proper */
CREATE INDEX idx_hash_tracking_number ON shipments USING hash(tracking_number);
	

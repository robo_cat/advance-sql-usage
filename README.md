# Optimizing database performance for Global Logistic Company

This project aims to optimize database performance to streamline the management of goods shipments across multiple warehouses with diverse carriers. 

## Setup and Installation

* Make sure you have a SQL database management system (DBMS) installed, such as PostgreSQL, MySQL, or SQLite.
* Use your preferred SQL client or command-line interface to execute the SQL queries, such as pgAdmin.
* Download the project from GitLab using git clone command
> git clone https://gitlab.com/robo_cat/advance-sql-usage.git
* Open the AdvanceSqlAssignment_BelginVatansever file in your preferred SQL client tool.

## Dependencies

The project has the following dependencies:

- [SQL Database Management System (DBMS)]: Choose a DBMS compatible with SQL, such as PostgreSQL, MySQL, SQLite, etc.
- [SQL Client]: Use a SQL client or command-line interface to execute SQL scripts and queries. Examples include pgAdmin, MySQL Workbench, DBeaver, SQL Server Management Studio, etc.

## Build and Deployment

Open the AdvanceSqlAssignment_BelginVatansever file and run the queries step by step that is defined in the file.

## Project Structure

1. __Views:__

* Create a view named warehouse_shipments that displays the tracking number, weight, and status of shipments along with the warehouse name for each shipment.
![alt text](<Screenshot 2024-03-12 at 15.08.12.png>)

* Create another view named carrier_shipments that shows the tracking number, weight, and status of shipments along with the carrier name for each shipment.
![alt text](<Screenshot 2024-03-12 at 15.08.33.png>)

2. __Common Table Expressions (CTEs):__

* Create a CTE named pending_shipments that includes the tracking number, weight, and warehouse location for all shipments with the status 'Pending'.
![alt text](<Screenshot 2024-03-12 at 15.16.47-1.png>)

* Create a CTE named heavy_shipments that includes the tracking number, weight, and carrier name for all shipments with a weight greater than 200.

1. First glance to the result ![alt text](<Screenshot 2024-03-12 at 15.12.03.png>)

2. After added a new entry over 200 units. ![alt text](<Screenshot 2024-03-12 at 15.18.58.png>)


3. __Transactions:__

* Write a transaction that updates the status of the shipment with tracking number 'ABC123' to 'Shipped' and increments the weight by 10 units.

1. Before the transaction a snapshot of the table
![alt text](<Screenshot 2024-03-12 at 15.21.53.png>)

2.  After transaction
![alt text](<Screenshot 2024-03-12 at 15.22.18.png>)


* Write another transaction that inserts a new shipment with tracking number 'LMN456', weight 180.75, into Warehouse B using Carrier Y.
![alt text](<Screenshot 2024-03-12 at 15.24.06.png>)

4. __Indexes:__

* Considers a frequently executed query to retrieve shipments based on their tracking number.
SELECT tracking_number, weight, status
FROM shipments
WHERE tracking_number = 'XYZ789';
This query is essential for tracking specific shipments. However, as the dataset grows, there might be performance concerns. Enhance the performance of this query by creating an appropriate index.
![alt text](<Screenshot 2024-03-12 at 15.45.16.png>)

## Authors

* Belgin Vatansever

## License

* This is an assigment for the SQL course. 

Copyright (c) 2024-2025 Scania Academy.
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Acknowledgments

Thanks to Sean Skinner for the great tutorial and for helping us learn SQL!